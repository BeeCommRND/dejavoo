"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
/**
 * pinpad device id
 * the terminal can send transactions to multiple devices
 * each of them have this unique pinpad code
 */
var pinpadCode = 35798860;
/**
 * the pinpad code formatted for the request.
 */
var track2 = "PINPAD" + pinpadCode;
/**
 * terminal number
 * long code number from shva
 */
var terminalNumber = "0882016016";
/**
 * terminal password
 * a constant password supplied by z-credit team for every terminal.
 */
var password = "Z0882016016";
var TransactionType;
(function (TransactionType) {
    TransactionType["Regular"] = "01";
    TransactionType["Refund"] = "53";
})(TransactionType || (TransactionType = {}));
/**
 * api base config
 */
var instance = axios_1.default.create({
    baseURL: "https://pci.zcredit.co.il/ZCreditWS/api/",
    timeout: 1000 * 100, // 100 seconds - let the client regenerate his pin code 🤷‍♀️
});
/**
 * complete transaction
 * See also {@link https://zcreditws.docs.apiary.io/#reference/0/validate-a-card/commitfulltransaction-with-invoice}
 */
var commitfulltransaction = function (transaction) { return __awaiter(void 0, void 0, void 0, function () {
    var data, response, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, instance.post("/Transaction/CommitFullTransaction", transaction)];
            case 1:
                data = (_a.sent()).data;
                response = data;
                if (!response.HasError) {
                    console.log("Success! " + transaction.transactionSum + "$ payed with card " + response.CardNumber);
                }
                else {
                    console.log("Error: " + response.ReturnMessage);
                }
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                console.log(error_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var defaultTransaction = {
    terminalNumber: terminalNumber,
    password: password,
    track2: track2,
    transactionType: TransactionType.Regular,
    transactionSum: 1.0 * 100, // 100.0 ILS
};
commitfulltransaction(defaultTransaction);
