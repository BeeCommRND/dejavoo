export interface TransactionResult {
  HasError: boolean;
  ReturnCode: number;
  ReturnMessage: string;
  CardNumber: string;
  ExpDate_MMYY: string;
  CVV: string;
  CardName: string;
  CardIssuerCode: string;
  CardFinancerCode: string;
  CardBrandCode: string;
  ReferenceNumber: number;
  VoucherNumber: string;
  ApprovalNumber: string;
  ApprovalType: string;
  NotePrintData: string;
  NotePrintDataSeller?: null;
  ResultRecord: string;
  IntOt_JSON: string;
  IntOt: string;
  TraceGUID?: null;
  IsTelApprovalNeeded: boolean;
  Token: string;
  Logs?: null[] | null;
  ClientReciept: string;
  SellerReciept: string;
  ClientRecieptPP: string;
  SellerRecieptPP: string;
  SignatureData: string;
  DspBalance?: null;
  PinpadCommunication: any;
  IntIns: any;
  IsPinpadRequested: boolean;
  ZCreditInvoiceReceiptResponse?: null;
  ZCreditPinpadReport: any;
  NoteLink?: null;
  PanEntryMode: string;
}
