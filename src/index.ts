import axios from "axios";
import { TransactionResult } from "./Dejavoo";

/**
 * gateway portal for viewing transactions:
 * {@link https://pci.zcredit.co.il/WebControl/Login.aspx}
 */

/**
 * pinpad device id
 * the terminal can send transactions to multiple devices
 * each of them have this unique pinpad code
 */
const pinpadCode: number = 12345678;

/**
 * the pinpad code formatted for the request.
 */
const track2: string = `PINPAD${pinpadCode}`;

/**
 * terminal number
 * long code number from shva
 */
const terminalNumber: string = "REPLACE_TERMINAL_NUMBER";

/**
 * terminal password
 * a constant password supplied by z-credit team for every terminal.
 */
const password: string = "REPLACE_TERMINAL_PASSWORD";

enum TransactionType {
  Regular = "01",
  Refund = "53",
}

/**
 * api base config
 */
const instance = axios.create({
  baseURL: "https://pci.zcredit.co.il/ZCreditWS/api/",
  timeout: 1000 * 100, // 100 seconds - let the client regenerate his pin code 🤷‍♀️
});

/**
 * complete transaction
 * See also {@link https://zcreditws.docs.apiary.io/#reference/0/validate-a-card/commitfulltransaction-with-invoice}
 */
const commitfulltransaction = async (transaction: any) => {
  try {
    const { data } = await instance.post(
      "/Transaction/CommitFullTransaction",
      transaction
    );

    const response = data as TransactionResult;

    if (!response.HasError) {
      console.log(`Success! ${transaction.transactionSum}$ payed with card ${response.CardNumber}`)
    } else {
      console.log(`Error: ${response.ReturnMessage}`)
    }
    
  } catch (error) {
    console.log(error);
  }
};

const defaultTransaction: any = {
  terminalNumber,
  password,
  track2,
  transactionType: TransactionType.Regular,
  transactionSum: 1.0, // 1.0 ILS
};

commitfulltransaction(defaultTransaction);